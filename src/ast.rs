use std::collections::HashMap;

// HACK Hash{Map,Set} causes nondeterminism

#[derive(Debug, Clone)]
pub struct Program {
    pub name: String,
    pub callables: HashMap<String, Callable>,
    pub consts: HashMap<String, Expr>,
    pub globals: HashMap<String, Type>,
    pub stmts: Vec<Stmt>,
}

#[derive(Debug, Clone)]
pub struct Callable {
    pub sig: Signature,
    pub locals: HashMap<String, Type>,
    pub stmts: Vec<Stmt>,
}

#[derive(Debug, Clone)]
pub struct Signature {
    pub params: Vec<(String, Type)>,
    pub function: bool,
}

#[derive(Debug, Clone)]
pub enum Type {
    Integer,
    /// first, last
    Array(i64, i64), // HACK there are some unchecked casts in codegen involving these, it's a bad idea
}

#[derive(Debug, Clone)]
pub enum Stmt {
    /// text to write
    Write(String),
    /// expr to write
    WriteLn(Expr),
    /// var to read into
    ReadLn(String),
    /// fn or proc name, arguments
    Call(String, Vec<Expr>),
    /// name, value
    AssignVar(String, Expr),
    /// name, index, value
    AssignArr(String, Expr, Expr),
    /// vars, statements
    Block(HashMap<String, Type>, Vec<Stmt>),
    /// condition, then branch, else branch
    If(Expr, Box<Stmt>, Box<Stmt>),
    /// condition, statements
    While(Expr, Box<Stmt>),
    /// variable, first, dir, last (inclusive), block
    For(String, Expr, i64, Expr, Box<Stmt>),

    Break,

    Continue,

    Exit,
}

#[derive(Debug, Clone)]
pub enum Expr {
    Const(i64),
    Neg(Box<Expr>),
    Add(Box<Expr>, Box<Expr>),
    Sub(Box<Expr>, Box<Expr>),
    Mul(Box<Expr>, Box<Expr>),
    Div(Box<Expr>, Box<Expr>),
    Mod(Box<Expr>, Box<Expr>),

    Less(Box<Expr>, Box<Expr>),
    LessEquals(Box<Expr>, Box<Expr>),
    Greater(Box<Expr>, Box<Expr>),
    GreaterEquals(Box<Expr>, Box<Expr>),
    NotEquals(Box<Expr>, Box<Expr>),
    Equals(Box<Expr>, Box<Expr>),

    Not(Box<Expr>),
    And(Box<Expr>, Box<Expr>),
    Or(Box<Expr>, Box<Expr>),

    /// variable/constant name
    Ref(String),
    /// array name, index
    ArrRef(String, Box<Expr>),
    /// function name, arguments
    FnCall(String, Vec<Expr>),
}

impl Expr {
    // TODO this could be used for optimization, not just for testing
    pub fn eval(&self) -> i64 {
        match *self {
            Expr::Const(n) => n,
            Expr::Neg(ref x) => -x.eval(),
            Expr::Add(ref l, ref r) => l.eval() + r.eval(),
            Expr::Sub(ref l, ref r) => l.eval() - r.eval(),
            Expr::Mul(ref l, ref r) => l.eval() * r.eval(),
            Expr::Div(ref l, ref r) => l.eval() / r.eval(),
            Expr::Mod(ref l, ref r) => l.eval() % r.eval(),

            Expr::Less(ref l, ref r) => (l.eval() < r.eval()) as i64,
            Expr::LessEquals(ref l, ref r) => (l.eval() <= r.eval()) as i64,
            Expr::Greater(ref l, ref r) => (l.eval() > r.eval()) as i64,
            Expr::GreaterEquals(ref l, ref r) => (l.eval() >= r.eval()) as i64,
            Expr::NotEquals(ref l, ref r) => (l.eval() != r.eval()) as i64,
            Expr::Equals(ref l, ref r) => (l.eval() == r.eval()) as i64,

            Expr::Not(ref x) => {
                println!("{}", x.eval());
                println!("{}", x.eval() == 0);
                println!("{}", (x.eval() == 0) as i64);
                (x.eval() == 0) as i64
            }
            Expr::And(ref l, ref r) => (l.eval() != 0 && r.eval() != 0) as i64,
            Expr::Or(ref l, ref r) => (l.eval() != 0 || r.eval() != 0) as i64,

            Expr::Ref(_) => {
                println!("Can't eval Ref at compile time");
                panic!()
            }
            Expr::ArrRef(..) => {
                println!("Can't eval ArrRef at compile time");
                panic!()
            }
            Expr::FnCall(..) => {
                println!("Can't eval FnCall at compile time");
                panic!()
            }
        }
    }
}
