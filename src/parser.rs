use std::collections::{HashMap, HashSet};
use std::iter::Peekable;

use lexer::{Lexer, Token};
use ast::{Program, Callable, Signature, Type, Stmt, Expr};

pub struct Parser<'a> {
    lexer: &'a mut Peekable<Lexer<'a>>,
    token: Token,
}

impl<'a> Parser<'a> {
    pub fn new(lexer: &'a mut Peekable<Lexer<'a>>) -> Self {
        let tok = lexer.next().unwrap();
        Parser { lexer: lexer, token: tok }
    }

    fn invalid(&mut self) -> ! {
        println!("Got invalid token: {:?}", self.token);
        println!("Following tokens:");
        for tok in self.lexer.take(5) {
            println!("{:?}", tok);
        }
        panic!();
    }

    fn advance(&mut self) {
        self.token = self.lexer.next().unwrap();
    }

    fn advance_string(&mut self) -> String {
        match self.token.clone() {
            Token::Ident(ident) => {
                self.token = self.lexer.next().unwrap();
                ident
            }
            Token::StringLit(s) => {
                self.token = self.lexer.next().unwrap();
                s
            }
            _ => unreachable!(),
        }
    }

    fn compare(&mut self, expected: Token) {
        if self.token != expected {
            println!("{:?} != {:?}", self.token, expected);
            println!("Following tokens:");
            for tok in self.lexer.take(5) {
                println!("{:?}", tok);
            }
            panic!();
        }
        self.advance();
    }

    fn compare_ident(&mut self) -> String {
        match self.token {
            Token::Ident(_) => self.advance_string(),
            _ => self.invalid(),
        }
    }

    fn compare_int_lit(&mut self) -> i64 {
        match self.token {
            Token::IntLit(n) => {
                self.advance();
                n
            }
            _ => self.invalid(),
        }
    }

    fn compare_string_lit(&mut self) -> String {
        match self.token {
            Token::StringLit(_) => self.advance_string(),
            _ => self.invalid(),
        }
    }

    pub fn parse(&mut self) -> Program {
        match self.token {
            Token::KwProgram => {
                self.advance();

                let name = self.compare_ident();

                self.compare(Token::Semi);

                let consts = self.consts();

                let mut callables = HashMap::new();
                self.fns_procs(&mut callables);

                let (globals, stmts) = self.block();

                self.compare(Token::Dot);

                Program {
                    name,
                    callables,
                    consts,
                    globals,
                    stmts,
                }
            }
            _ => self.invalid(),
        }
    }

    fn consts(&mut self) -> HashMap<String, Expr> {
        match self.token {
            Token::KwFunction | Token::KwProcedure | Token::KwBegin | Token::KwVar => HashMap::new(),
            Token::KwConst => {
                self.advance();
                let mut consts = HashMap::new();
                let (ident, expr) = self.const_assign();
                consts.insert(ident, expr);
                self.const_assigns(&mut consts);
                consts
            }
            _ => self.invalid(),
        }
    }

    fn const_assigns(&mut self, consts: &mut HashMap<String, Expr>) {
        match self.token {
            Token::Ident(_) => {
                let (ident, expr) = self.const_assign();
                consts.insert(ident, expr);
                self.const_assigns(consts);
            }
            Token::KwFunction | Token::KwProcedure | Token::KwBegin => {}
            Token::KwVar => {}
            _ => self.invalid(),
        }
    }

    fn const_assign(&mut self) -> (String, Expr) {
        match self.token {
            Token::Ident(_) => {
                let ident = self.advance_string();
                self.compare(Token::Equals);
                let expr = self.expr();
                self.compare(Token::Semi);
                (ident, expr)
            }
            _ => self.invalid(),
        }
    }

    fn vars(&mut self, vars: &mut HashMap<String, Type>) {
        match self.token {
            Token::KwBegin => {}
            Token::KwVar => {
                self.var(vars);
                self.vars(vars);
            }
            _ => self.invalid(),
        }
    }

    fn var(&mut self, vars: &mut HashMap<String, Type>) {
        match self.token {
            Token::KwVar => {
                self.advance();
                self.var_decls(vars);
            }
            _ => self.invalid(),
        }
    }

    fn var_decls(&mut self, vars: &mut HashMap<String, Type>) {
        match self.token {
            Token::Ident(_) => {
                self.var_decl(vars);
                self.var_decls(vars);
            }
            Token::KwBegin | Token::KwVar => {}
            _ => self.invalid(),
        }
    }

    fn var_decl(&mut self, vars: &mut HashMap<String, Type>) {
        match self.token {
            Token::Ident(_) => {
                let names = self.idents();
                self.compare(Token::Colon);
                let ty = self.type_name();
                self.compare(Token::Semi);
                for name in names {
                    vars.insert(name, ty.clone());
                }
            }
            _ => self.invalid(),
        }
    }

    fn type_name(&mut self) -> Type {
        match self.token {
            Token::KwInteger => {
                self.advance();
                Type::Integer
            }
            Token::KwArray => {
                self.advance();
                self.compare(Token::LSquare);
                let lowest_index = self.expr().eval();
                self.compare(Token::DotDot);
                let highest_index = self.expr().eval();
                assert!(lowest_index <= highest_index); // nice user readable error message ;)
                self.compare(Token::RSquare);
                self.compare(Token::KwOf);
                self.compare(Token::KwInteger);
                Type::Array(lowest_index, highest_index)
            }
            _ => self.invalid(),
        }
    }

    fn idents(&mut self) -> HashSet<String> {
        match self.token {
            Token::Ident(_) => {
                let ident = self.advance_string();
                let mut names = HashSet::new();
                names.insert(ident);
                self.more_idents(&mut names);
                names
            }
            _ => self.invalid(),
        }
    }

    fn more_idents(&mut self, names: &mut HashSet<String>) {
        match self.token {
            Token::Colon => {}
            Token::Comma => {
                self.advance();
                let name = self.compare_ident();
                names.insert(name);
                self.more_idents(names);
            }
            _ => self.invalid(),
        }
    }

    fn fns_procs(&mut self, callables: &mut HashMap<String, Callable>) {
        match self.token {
            Token::KwFunction => {
                self.fn_(callables);
                self.fns_procs(callables);
            }
            Token::KwProcedure => {
                self.proc_(callables);
                self.fns_procs(callables);
            }
            Token::KwBegin | Token::KwVar => {}
            _ => self.invalid(),
        }
    }

    fn fn_(&mut self, callables: &mut HashMap<String, Callable>) {
        match self.token {
            Token::KwFunction => {
                let (name, sig) = self.fn_sig();
                if let Some((locals, stmts)) = self.fwd_or_real() {
                    callables.insert(name, Callable { sig, locals, stmts });
                } else {
                    eprintln!("WARNING: ignoring forward declaration");
                }
            }
            _ => self.invalid(),
        }
    }

    fn fn_sig(&mut self) -> (String, Signature) {
        match self.token {
            Token::KwFunction => {
                self.advance();
                let fn_name = self.compare_ident();
                self.compare(Token::LParen);
                let params = self.params();
                self.compare(Token::RParen);
                self.compare(Token::Colon);
                self.compare(Token::KwInteger);
                self.compare(Token::Semi);
                (fn_name, Signature { params, function: true })
            }
            _ => self.invalid(),
        }
    }

    fn proc_(&mut self, callables: &mut HashMap<String, Callable>) {
        match self.token {
            Token::KwProcedure => {
                let (name, sig) = self.proc_sig();
                if let Some((locals, stmts)) = self.fwd_or_real() {
                    callables.insert(name, Callable { sig, locals, stmts });
                } else {
                    eprintln!("WARNING: ignoring forward declaration");
                }
            }
            _ => self.invalid(),
        }
    }

    fn proc_sig(&mut self) -> (String, Signature) {
        match self.token {
            Token::KwProcedure => {
                self.advance();
                let proc_name = self.compare_ident();
                self.compare(Token::LParen);
                let params = self.params();
                self.compare(Token::RParen);
                self.compare(Token::Semi);
                (proc_name, Signature { params, function: false })
            }
            _ => self.invalid(),
        }
    }

    fn fwd_or_real(&mut self) -> Option<(HashMap<String, Type>, Vec<Stmt>)> {
        match self.token {
            Token::KwForward => {
                self.advance();
                self.compare(Token::Semi);
                None
            }
            Token::KwBegin | Token::KwVar => {
                let (locals, stmts) = self.block();
                self.compare(Token::Semi);
                Some((locals, stmts))
            }
            _ => self.invalid(),
        }
    }

    fn params(&mut self) -> Vec<(String, Type)> {
        let mut params = Vec::new();
        match self.token {
            Token::RParen => {}
            Token::Ident(_) => {
                params.push(self.param());
                self.more_params(&mut params);
            }
            _ => self.invalid(),
        }
        params
    }

    fn more_params(&mut self, params: &mut Vec<(String, Type)>) {
        match self.token {
            Token::RParen => {}
            Token::Semi => {
                self.advance();
                params.push(self.param());
                self.more_params(params);
            }
            _ => self.invalid(),
        }
    }

    fn param(&mut self) -> (String, Type) {
        match self.token {
            Token::Ident(_) => {
                let name = self.advance_string();
                self.compare(Token::Colon);
                self.compare(Token::KwInteger);
                (name, Type::Integer)
            }
            _ => self.invalid(),
        }
    }

    fn block(&mut self) -> (HashMap<String, Type>, Vec<Stmt>) {
        match self.token {
            Token::KwBegin | Token::KwVar => {
                let mut vars = HashMap::new();
                self.vars(&mut vars);
                self.advance();
                let mut stmts = Vec::new();
                self.stmts(&mut stmts);
                self.compare(Token::KwEnd);
                (vars, stmts)
            }
            _ => self.invalid(),
        }
    }

    fn stmts(&mut self, stmts: &mut Vec<Stmt>) {
        match self.token {
            Token::Ident(_) | Token::KwBegin | Token::KwIf | Token::KwWhile | Token::KwFor
            | Token::KwBreak | Token::KwContinue | Token::KwVar
            | Token::KwWrite | Token::KwWriteLn | Token::KwReadLn | Token::KwExit => {
                stmts.push(self.stmt());
                self.stmts(stmts);
            }
            Token::KwEnd => {}
            _ => self.invalid(),
        }
    }

    fn stmt(&mut self) -> Stmt {
        match self.token {
            Token::Ident(_) => {
                let name = self.advance_string();
                let stmt = self.assign_or_call(name);
                self.opt_semi();
                stmt
            }
            Token::KwBegin | Token::KwVar => {
                let (vars, stmts) = self.block();
                self.opt_semi();
                Stmt::Block(vars, stmts)
            }
            Token::KwIf => self.if_(),
            Token::KwWhile => self.while_(),
            Token::KwFor => self.for_(),
            Token::KwBreak => {
                self.advance();
                self.opt_semi();
                Stmt::Break
            }
            Token::KwContinue => {
                self.advance();
                self.opt_semi();
                Stmt::Continue
            }
            Token::KwWrite => {
                let text = self.write();
                self.opt_semi();
                Stmt::Write(text)
            }
            Token::KwWriteLn => {
                let stmt = self.writeln();
                self.opt_semi();
                stmt
            }
            Token::KwReadLn => {
                let stmt = self.readln();
                self.opt_semi();
                stmt
            }
            Token::KwExit => {
                self.advance();
                self.opt_semi();
                Stmt::Exit
            }
            _ => self.invalid(),
        }
    }

    fn opt_semi(&mut self) {
        match self.token {
            Token::Semi => self.advance(),
            Token::Ident(_) | Token::KwBegin | Token::KwEnd | Token::KwIf | Token::KwElse
            | Token::KwWhile | Token::KwFor
            | Token::KwBreak | Token::KwContinue | Token::KwVar
            | Token::KwWrite | Token::KwWriteLn | Token::KwReadLn
            | Token::KwExit => {}
            _ => self.invalid(),
        }
    }

    fn write(&mut self) -> String {
        match self.token {
            Token::KwWrite => {
                self.advance();
                self.compare(Token::LParen);
                let text = self.compare_string_lit();
                self.compare(Token::RParen);
                text
            }
            _ => self.invalid(),
        }
    }

    fn writeln(&mut self) -> Stmt {
        match self.token {
            Token::KwWriteLn => {
                self.advance();
                self.compare(Token::LParen);
                let expr = self.expr();
                self.compare(Token::RParen);
                Stmt::WriteLn(expr)
            }
            _ => self.invalid(),
        }
    }

    fn readln(&mut self) -> Stmt {
        match self.token {
            Token::KwReadLn => {
                self.advance();
                self.compare(Token::LParen);
                let var_name = self.compare_ident();
                self.compare(Token::RParen);
                Stmt::ReadLn(var_name)
            }
            _ => self.invalid(),
        }
    }

    fn assign_or_call(&mut self, name: String) -> Stmt {
        match self.token {
            Token::ColonEquals => {
                let expr = self.assign_var();
                Stmt::AssignVar(name, expr)
            }
            Token::LParen => {
                self.advance();
                let args = self.args();
                self.compare(Token::RParen);
                Stmt::Call(name, args)
            }
            Token::LSquare => {
                let (index, value) = self.assign_arr();
                Stmt::AssignArr(name, index, value)
            }
            _ => self.invalid(),
        }
    }

    fn assign_var(&mut self) -> Expr {
        match self.token {
            Token::ColonEquals => {
                self.advance();
                self.expr()
            }
            _ => self.invalid(),
        }
    }

    fn assign_arr(&mut self) -> (Expr, Expr) {
        match self.token {
            Token::LSquare => {
                self.advance();
                let index = self.expr();
                self.compare(Token::RSquare);
                self.compare(Token::ColonEquals);
                let value = self.expr();
                (index, value)
            }
            _ => self.invalid(),
        }
    }

    fn if_(&mut self) -> Stmt {
        match self.token {
            Token::KwIf => {
                self.advance();
                let expr = self.expr();
                self.compare(Token::KwThen);
                let then_branch = self.stmt();
                let else_branch = self.else_();
                Stmt::If(expr, Box::new(then_branch), Box::new(else_branch))
            }
            _ => self.invalid(),
        }
    }

    fn else_(&mut self) -> Stmt {
        match self.token {
            Token::Ident(_) | Token::KwBegin | Token::KwEnd
            | Token::KwIf | Token::KwWhile | Token::KwFor
            | Token::KwBreak | Token::KwContinue | Token::KwVar
            | Token::KwWrite | Token::KwWriteLn | Token::KwReadLn | Token::KwExit => {
                Stmt::Block(HashMap::new(), vec![])
            }
            Token::KwElse => {
                self.advance();
                self.stmt()
            }
            _ => self.invalid(),
        }
    }

    fn while_(&mut self) -> Stmt {
        match self.token {
            Token::KwWhile => {
                self.advance();
                let expr = self.expr();
                self.compare(Token::KwDo);
                let block = self.stmt();
                Stmt::While(expr, Box::new(block))
            }
            _ => self.invalid(),
        }
    }

    fn for_(&mut self) -> Stmt {
        match self.token {
            Token::KwFor => {
                self.advance();
                let ident = self.compare_ident();
                self.compare(Token::ColonEquals);
                let first = self.expr();
                let dir = self.for_dir();
                let last = self.expr();
                self.compare(Token::KwDo);
                let block = self.stmt();
                Stmt::For(ident, first, dir, last, Box::new(block))
            }
            _ => self.invalid(),
        }
    }

    fn for_dir(&mut self) -> i64 {
        match self.token {
            Token::KwTo => {
                self.advance();
                1
            }
            Token::KwDownTo => {
                self.advance();
                -1
            }
            _ => self.invalid(),
        }
    }

    fn expr(&mut self) -> Expr {
        match self.token {
            Token::Minus => {
                self.advance();
                let lhs = self.term();
                self.expr1(Expr::Neg(Box::new(lhs)))
            }
            Token::LParen | Token::IntLit(_) | Token::Ident(_) => {
                let lhs = self.term();
                self.expr1(lhs)
            }
            Token::KwNot => {
                self.advance();
                let lhs = self.term();
                self.expr1(Expr::Not(Box::new(lhs)))
            }
            _ => self.invalid(),
        }
    }

    fn expr1(&mut self, lhs: Expr) -> Expr {
        match self.token {
            Token::Plus => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::Add(Box::new(lhs), Box::new(rhs)))
            }
            Token::Minus => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::Sub(Box::new(lhs), Box::new(rhs)))
            }
            Token::Less => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::Less(Box::new(lhs), Box::new(rhs)))
            }
            Token::LessEquals => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::LessEquals(Box::new(lhs), Box::new(rhs)))
            }
            Token::Greater => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::Greater(Box::new(lhs), Box::new(rhs)))
            }
            Token::GreaterEquals => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::GreaterEquals(Box::new(lhs), Box::new(rhs)))
            }
            Token::LessGreater => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::NotEquals(Box::new(lhs), Box::new(rhs)))
            }
            Token::Equals => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::Equals(Box::new(lhs), Box::new(rhs)))
            }
            Token::RParen | Token::RSquare | Token::Semi | Token::Comma | Token::DotDot
            | Token::Ident(_) | Token::KwBegin | Token::KwEnd
            | Token::KwIf | Token::KwThen | Token::KwElse
            | Token::KwWhile | Token::KwFor | Token::KwDo
            | Token::KwBreak | Token::KwContinue | Token::KwVar
            | Token::KwTo | Token::KwDownTo
            | Token::KwWrite | Token::KwWriteLn | Token::KwReadLn | Token::KwExit => {
                lhs
            }
            Token::KwOr => {
                self.advance();
                let rhs = self.term();
                self.expr1(Expr::Or(Box::new(lhs), Box::new(rhs)))
            }
            _ => self.invalid(),
        }
    }

    fn term(&mut self) -> Expr {
        match self.token {
            Token::LParen | Token::IntLit(_) | Token::Ident(_) => {
                let lhs = self.factor();
                self.term1(lhs)
            }
            _ => self.invalid(),
        }
    }

    fn term1(&mut self, lhs: Expr) -> Expr {
        match self.token {
            Token::Plus | Token::Minus
            | Token::Less | Token::LessEquals | Token::Greater | Token::GreaterEquals
            | Token::LessGreater | Token::Equals
            | Token::RParen | Token::RSquare | Token::Semi | Token::Comma | Token::DotDot
            | Token::Ident(_) | Token::KwBegin | Token::KwEnd
            | Token::KwIf | Token::KwThen | Token::KwElse
            | Token::KwWhile | Token::KwFor | Token::KwDo
            | Token::KwBreak | Token::KwContinue | Token::KwVar
            | Token::KwTo | Token::KwDownTo
            | Token::KwWrite | Token::KwWriteLn | Token::KwReadLn
            | Token::KwOr
            | Token::KwExit => {
                lhs
            }
            Token::Times => {
                self.advance();
                let rhs = self.factor();
                self.term1(Expr::Mul(Box::new(lhs), Box::new(rhs)))
            }
            Token::Slash | Token::KwDiv => {
                self.advance();
                let rhs = self.factor();
                self.term1(Expr::Div(Box::new(lhs), Box::new(rhs)))
            }
            Token::KwMod => {
                self.advance();
                let rhs = self.factor();
                self.term1(Expr::Mod(Box::new(lhs), Box::new(rhs)))
            }
            Token::KwAnd => {
                self.advance();
                let rhs = self.factor();
                self.term1(Expr::And(Box::new(lhs), Box::new(rhs)))
            }
            _ => self.invalid(),
        }
    }

    fn factor(&mut self) -> Expr {
        match self.token {
            Token::LParen => {
                self.advance();
                let nested = self.expr();
                self.compare(Token::RParen);
                nested
            }
            Token::IntLit(_) => {
                Expr::Const(self.compare_int_lit())
            }
            Token::Ident(_) => {
                let ident = self.compare_ident();
                self.ref_or_fn_or_arr(ident)
            }
            _ => self.invalid(),
        }
    }

    fn ref_or_fn_or_arr(&mut self, ident: String) -> Expr {
        match self.token {
            Token::Plus | Token::Minus | Token::Times | Token::Slash
            | Token::Less | Token::LessEquals | Token::Greater | Token::GreaterEquals
            | Token::LessGreater | Token::Equals
            | Token::RParen | Token::RSquare | Token::Semi | Token::Comma | Token::DotDot
            | Token::Ident(_) | Token::KwBegin | Token::KwEnd
            | Token::KwIf | Token::KwThen | Token::KwElse
            | Token::KwWhile | Token::KwFor | Token::KwDo
            | Token::KwBreak | Token::KwContinue | Token::KwVar
            | Token::KwTo | Token::KwDownTo
            | Token::KwWrite | Token::KwWriteLn | Token::KwReadLn
            | Token::KwMod | Token::KwDiv | Token::KwAnd | Token::KwOr
            | Token::KwExit => {
                Expr::Ref(ident)
            }
            Token::LParen => {
                self.advance();
                let args = self.args();
                self.compare(Token::RParen);
                Expr::FnCall(ident, args)
            }
            Token::LSquare => {
                self.advance();
                let index = self.expr();
                self.compare(Token::RSquare);
                // HACK this is the place where i should be offsetting indexes, not codegen
                Expr::ArrRef(ident, Box::new(index))
            }
            _ => self.invalid(),
        }
    }

    fn args(&mut self) -> Vec<Expr> {
        let mut exprs = Vec::new();
        match self.token {
            Token::Minus | Token::LParen | Token::IntLit(_) | Token::Ident(_) | Token::KwNot => {
                exprs.push(self.expr());
                self.more_args(&mut exprs);
            }
            Token::RParen => {}
            _ => self.invalid(),
        }
        exprs
    }

    fn more_args(&mut self, exprs: &mut Vec<Expr>) {
        match self.token {
            Token::RParen => {}
            Token::Comma => {
                self.advance();
                exprs.push(self.expr());
                self.more_args(exprs);
            }
            _ => self.invalid(),
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::Path;
    use super::*;
    use utils;

    #[test]
    fn parse_samples() {
        use std::fs;

        for entry in fs::read_dir("samples").unwrap() {
            let path = entry.unwrap().path();
            println!("Testing {}", path.to_str().unwrap());
            parse_file(path.to_str().unwrap());
        }
    }

    #[test]
    fn parse_my_samples() {
        use std::fs;

        for entry in fs::read_dir("my_samples").unwrap() {
            let path = entry.unwrap().path();
            if path.file_name().unwrap().to_str().unwrap().contains("bad") { continue; } // HACK
            println!("Testing {}", path.to_str().unwrap());
            parse_file(path);
        }
    }

    fn parse_file<P: AsRef<Path>>(path: P) {
        let source = utils::load_file(path).unwrap();
        parse_code(&source);
    }

    fn parse_code(code: &str) {
        let mut lexer = Lexer::from_code(code).peekable();
        let mut parser = Parser::new(&mut lexer);
        parser.parse();
    }

    #[test]
    fn eval_expressions() {
        check_expr("1", 1);
        check_expr("1-2", -1);
        check_expr("100-50-20-10-5-1", 14);
        check_expr("100-50/5/2-4", 91);

        check_expr("(0)", 0);
        check_expr("256/2/(200-72)", 1);

        check_expr("3+5", 8);
        check_expr("1+5*10/2", 26);

        check_expr("99 div 11 / 3 + 2", 5);

        check_expr("-10", -10);
        check_expr("-10-5-3", -18);
        check_expr("9+(-4)*6/(-3)", 17);

        check_expr("9 mod 2", 1);
        check_expr("1 = 1", 1);
        check_expr("5 < (3+3)", 1);
        check_expr("2+2 <> 4", 0);

        check_expr("(8-18)*5", -50);
        check_expr("-1/(-1)", 1);
        check_expr("-1/1", -1);

        check_expr("not 0", 1);
        check_expr("not 1", 0);

        check_expr("0 and 0", 0);
        check_expr("0 and 1", 0);
        check_expr("1 and 0", 0);
        check_expr("1 or 1", 1);

        check_expr("0 or 0", 0);
        check_expr("0 or 1", 1);
        check_expr("1 or 0", 1);
        check_expr("1 or 1", 1);

        check_expr("not 0 and (not (not 1))", 1);

        // ^ keep in sync with tests in lib
    }

    fn check_expr(source_expr: &str, result: i64) {
        // expressions need to end with a comma (or something else from their FOLLOW set)
        let expr = format!("{},", source_expr);
        let mut lexer = Lexer::from_code(&expr).peekable();
        let mut parser = Parser::new(&mut lexer);
        assert_eq!(parser.expr().eval(), result);
    }
}
