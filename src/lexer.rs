use std::collections::HashMap;
use std::iter::Peekable;
use std::str::Chars;

#[derive(Debug, Clone, PartialEq)]
pub enum Token {
    Plus,
    Minus,
    Times,
    Slash,

    Less,
    LessEquals,
    Greater,
    GreaterEquals,
    LessGreater,
    Equals,
    ColonEquals,

    LParen,
    RParen,
    LSquare,
    RSquare,

    Colon,
    Semi,
    Comma,
    Dot,
    DotDot,

    StringLit(String),
    IntLit(i64),

    Ident(String),

    KwProgram,
    KwFunction,
    KwProcedure,
    KwForward,
    KwBegin,
    KwEnd,

    KwIf,
    KwThen,
    KwElse,

    KwWhile,
    KwFor,
    KwDo,
    KwBreak,
    KwContinue,

    KwTo,
    KwDownTo,

    KwConst,
    KwVar,
    KwInteger,
    KwArray,
    KwOf,

    KwWrite,
    KwWriteLn,
    KwReadLn,

    KwMod,
    KwDiv,

    KwNot,
    KwAnd,
    KwOr,

    KwExit,

    Eof,
}

pub struct Lexer<'a> {
    iter: Peekable<Chars<'a>>, // TODO srsly, who called it iter when the lexer is an Iterator itself too
    kws: HashMap<&'static str, Token>,
}

impl<'a> Lexer<'a> {
    fn get_keywords() -> HashMap<&'static str, Token> {
        let mut keywords = HashMap::new();
        keywords.insert("program", Token::KwProgram);
        keywords.insert("procedure", Token::KwProcedure);
        keywords.insert("function", Token::KwFunction);
        keywords.insert("forward", Token::KwForward);
        keywords.insert("begin", Token::KwBegin);
        keywords.insert("end", Token::KwEnd);
        keywords.insert("if", Token::KwIf);
        keywords.insert("then", Token::KwThen);
        keywords.insert("else", Token::KwElse);
        keywords.insert("while", Token::KwWhile);
        keywords.insert("for", Token::KwFor);
        keywords.insert("do", Token::KwDo);
        keywords.insert("break", Token::KwBreak);
        keywords.insert("continue", Token::KwContinue);
        keywords.insert("to", Token::KwTo);
        keywords.insert("downto", Token::KwDownTo);
        keywords.insert("const", Token::KwConst);
        keywords.insert("var", Token::KwVar);
        keywords.insert("array", Token::KwArray);
        keywords.insert("of", Token::KwOf);
        keywords.insert("write", Token::KwWrite);
        keywords.insert("writeln", Token::KwWriteLn);
        keywords.insert("readln", Token::KwReadLn);
        keywords.insert("mod", Token::KwMod);
        keywords.insert("div", Token::KwDiv);
        keywords.insert("not", Token::KwNot);
        keywords.insert("and", Token::KwAnd);
        keywords.insert("or", Token::KwOr);
        keywords.insert("integer", Token::KwInteger);
        keywords.insert("exit", Token::KwExit);
        keywords
    }

    pub fn from_code(code: &'a str) -> Self {
        Lexer { iter: code.chars().peekable(), kws: Self::get_keywords() }
    }

    fn read_number_start(&mut self, base: u32) -> Option<Token> {
        // must have at least one digit
        match self.iter.next() {
            Some(n) => {
                match n.to_digit(base) {
                    Some(digit) => self.read_number(base, digit as i64),
                    None => None,
                }
            }
            None => None,
        }
    }

    fn read_number(&mut self, base: u32, mut total: i64) -> Option<Token> {
        // optionally more digits
        loop {
            match self.iter.peek() {
                Some(&n) => {
                    match n.to_digit(base) {
                        Some(digit) => {
                            self.iter.next();
                            let base = base as i64;
                            let digit = digit as i64;
                            let (acc, overflow1) = total.overflowing_mul(base);
                            let (acc, overflow2) = acc.overflowing_add(digit);
                            if overflow1 || overflow2 {
                                return None;
                            }
                            total = acc;
                        }
                        None => return Some(Token::IntLit(total)),
                    }
                }
                None => return Some(Token::IntLit(total)),
            }
        }
    }

    /// Opening quote is already consumed
    fn read_string(&mut self) -> Option<String> {
        let mut ret = String::new();
        loop {
            match self.iter.next() {
                Some('\'') => return Some(ret),
                Some('\\') => {
                    match self.iter.next() {
                        Some(c) => ret.push(c),
                        None => return None,
                    }
                }
                Some(c) => ret.push(c),
                None => return None,
            }
        }
    }

    fn read_ident_or_kw(&mut self, first_char: char) -> Token {
        let mut word = String::new();
        word.push(first_char);
        loop {
            match self.iter.peek() {
                Some(&c) if c.is_alphanumeric() || c == '_' => {
                    self.iter.next();
                    word.push(c);
                }
                _ => {
                    match self.kws.get(&*word) {
                        Some(token_kw) => return token_kw.clone(),
                        None => return Token::Ident(word)
                    }
                }
            }
        }
    }
}

impl<'a> Iterator for Lexer<'a> {
    type Item = Token;

    // TODO this is not completely correct - iterator should return None at the end, not keep returning Eof
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(&c) = self.iter.peek() {
            if c.is_whitespace() {
                self.iter.next();
            } else {
                break;
            }
        }
        // HACK all the Somes are ugly
        if let Some(c) = self.iter.next() {
            match c {
                '+' => Some(Token::Plus),
                '-' => Some(Token::Minus),
                '*' => Some(Token::Times),
                '/' => Some(Token::Slash),
                '<' => match self.iter.peek() {
                    Some(&n) if n == '=' => {
                        self.iter.next();
                        Some(Token::LessEquals)
                    }
                    Some(&n) if n == '>' => {
                        self.iter.next();
                        Some(Token::LessGreater)
                    }
                    _ => Some(Token::Less)
                },
                '>' => match self.iter.peek() {
                    Some(&n) if n == '=' => {
                        self.iter.next();
                        Some(Token::GreaterEquals)
                    }
                    _ => Some(Token::Greater)
                },
                '=' => Some(Token::Equals),
                ':' => match self.iter.peek() {
                    Some(&n) if n == '=' => {
                        self.iter.next();
                        Some(Token::ColonEquals)
                    }
                    _ => Some(Token::Colon)
                },
                '(' => Some(Token::LParen),
                ')' => Some(Token::RParen),
                '[' => Some(Token::LSquare),
                ']' => Some(Token::RSquare),
                ';' => Some(Token::Semi),
                ',' => Some(Token::Comma),
                '.' => match self.iter.peek() {
                    Some(&n) if n == '.' => {
                        self.iter.next();
                        Some(Token::DotDot)
                    }
                    _ => Some(Token::Dot)
                },
                '\'' => match self.read_string() {
                    Some(s) => Some(Token::StringLit(s)),
                    None => None,
                },
                '&' => self.read_number_start(8),
                '$' => self.read_number_start(16),
                c if c.is_numeric() => self.read_number(10, c.to_digit(10).unwrap() as i64),
                c if c.is_alphabetic() || c == '_' => Some(self.read_ident_or_kw(c)),
                c => {
                    println!("Unknown char: {}", c);
                    panic!()
                }
            }
        } else {
            Some(Token::Eof)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::Token::*;

    #[test]
    fn valid() {
        let source = r#"
+-*/
1 + 2 - 3 * 4 / 5+6-7*8/9
< <= > >= <> = :=
1<2
()[]
:;, : ; , . ..
5 &7 &0 $ff $a1a1a 666
'Factors of: ' '\'' '\\' 'a\\\'b'
const
qwertzuiop a555
'❤'
"#;

        let expected = [
            Plus,
            Minus,
            Times,
            Slash,
            IntLit(1),
            Plus,
            IntLit(2),
            Minus,
            IntLit(3),
            Times,
            IntLit(4),
            Slash,
            IntLit(5),
            Plus,
            IntLit(6),
            Minus,
            IntLit(7),
            Times,
            IntLit(8),
            Slash,
            IntLit(9),
            Less,
            LessEquals,
            Greater,
            GreaterEquals,
            LessGreater,
            Equals,
            ColonEquals,
            IntLit(1),
            Less,
            IntLit(2),
            LParen,
            RParen,
            LSquare,
            RSquare,
            Colon,
            Semi,
            Comma,
            Colon,
            Semi,
            Comma,
            Dot,
            DotDot,
            IntLit(5),
            IntLit(7),
            IntLit(0),
            IntLit(255),
            IntLit(662042),
            IntLit(666),
            StringLit("Factors of: ".to_owned()),
            StringLit("'".to_owned()),
            StringLit(r#"\"#.to_owned()),
            StringLit(r#"a\'b"#.to_owned()),
            KwConst,
            Ident("qwertzuiop".to_owned()),
            Ident("a555".to_owned()),
            StringLit("❤".to_owned()),
            Eof];

        let mut lexer = Lexer::from_code(source);
        for token in expected.iter() {
            let got = lexer.next();
            println!("Expecting: {:<width$} got: {:?}",
                     format!("{:?}", Some(token)),
                     got,
                     width = 25);
            assert_eq!(*token, got.unwrap());
        }
    }

    #[test]
    fn invalid() {
        let sources = ["&8", "&9", "$g", "'", r#"  '\'  "#,
            "9999999999999999999999999999999999", "&", "& 5"];
        for source in sources.iter() {
            let mut lexer = Lexer::from_code(source);
            let res = lexer.next();
            if let &Some(ref x) = &res {
                println!("{} -> {:?}", source, x);
            }
            assert_eq!(res, None);
        }
    }

    #[test]
    fn whitespace() {
        let mut source = " ".repeat(2_000_000);
        source.push_str("whitespace");
        let mut l = Lexer::from_code(&source);
        assert_eq!(l.next().unwrap(), Token::Ident("whitespace".to_owned()));
    }
}
