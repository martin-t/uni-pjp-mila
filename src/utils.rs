use std::error::Error;
use std::fs::File;
use std::io::Read;
use std::path::Path;

pub fn load_file<P: AsRef<Path>>(path: P) -> Result<String, Box<Error>> {
    let mut str = String::new();
    let mut file = File::open(path)?;
    file.read_to_string(&mut str)?;
    Ok(str)
}
