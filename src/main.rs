extern crate mila;

use std::env;
use std::process;

// TODO grep for unimplemented
// TODO ignored tests
// TODO reading from return "value" in functions???
// TODO try to overflow stack when evaluating or generating expressions
// TODO `cargo test run_factorial_` sometimes segfaults
// TODO test nested loops - break, continue

fn main() {
    let args: Vec<String> = env::args().collect();
    if args.len() != 2 {
        println!("Usage: mila <source file>");
        process::exit(1);
    }

    let path = &args[1];
    if let Err(err) = mila::compile_path(path) {
        println!("Error: {}", err);
    }
}
