extern crate llvm_sys;

use self::llvm_sys::*;
use self::llvm_sys::prelude::*;
use self::llvm_sys::core::*;
use self::llvm_sys::analysis::*;

use std::collections::HashMap;
use std::ffi::CString;
use std::os::raw::c_char;
use std::path::PathBuf;
use std::process::Command;
use std::ptr;

use ast::{Program, Callable, Type, Stmt, Expr};

const LLVM_FALSE: i32 = 0;
const LLVM_TRUE: i32 = 1;

#[derive(Debug, Clone)]
pub struct SymbolTable {
    scanf: LLVMValueRef,
    scanf_format: LLVMValueRef,
    printf: LLVMValueRef,
    printf_format: LLVMValueRef,
    consts: HashMap<String, Expr>,
    globals: HashMap<String, LLVMValueRef>,
    // HACK can we store LLVMValueRef instead of using LLVMGetNamedFunction for every call?
    callables: HashMap<String, Callable>,
    locals: HashMap<String, LLVMValueRef>,
}

// TODO can we get rid of cstrs?
pub struct Codegen {
    context: LLVMContextRef,
    module: LLVMModuleRef,
    builder: LLVMBuilderRef,
    cstrs: HashMap<String, CString>,
    break_dests: Vec<*mut LLVMBasicBlock>,
    continue_dests: Vec<*mut LLVMBasicBlock>,
    cur_fn_name: String,
}

impl Codegen {
    pub fn new() -> Codegen {
        let mod_name = CString::new("mila").unwrap();
        unsafe {
            let context = LLVMContextCreate();
            let module = LLVMModuleCreateWithName(mod_name.as_ptr());
            let builder = LLVMCreateBuilderInContext(context);

            Codegen {
                context,
                module,
                builder,
                cstrs: HashMap::new(),
                break_dests: Vec::new(),
                continue_dests: Vec::new(),
                cur_fn_name: "main".to_owned(),
            }
        }
    }

    fn cstr(&mut self, real_str: &str) -> *const c_char {
        // make sure the strings we pass to LLVM live long enough
        // do they even need to live after the call though? can't C just get lifetimes already? or docs :/

        // HACK this can't possibly be a good way to do it
        // edit: this makes no sense at all, the container can reallocate at any time

        let c_string = CString::new(real_str).unwrap();
        if !self.cstrs.contains_key(real_str) {
            self.cstrs.insert(real_str.to_owned(), c_string);
        }
        self.cstrs.get(real_str).unwrap().as_ptr()
    }

    fn prep_symbols(&mut self,
                    consts: HashMap<String, Expr>,
                    ast_globals: HashMap<String, Type>,
                    callables: &HashMap<String, Callable>) -> SymbolTable {
        unsafe {
            let mut scanf_param_types = [LLVMPointerType(LLVMInt8Type(), 0)];
            let scanf_type = LLVMFunctionType(
                LLVMInt32Type(),
                scanf_param_types.as_mut_ptr(),
                scanf_param_types.len() as u32,
                LLVM_TRUE,
            );
            let scanf = LLVMAddFunction(
                self.module,
                self.cstr("scanf"),
                scanf_type);
            let scanf_format = LLVMBuildGlobalStringPtr(
                self.builder,
                self.cstr("%lld"),
                self.cstr("__SCANF_FORMAT"),
            );

            let mut printf_param_types = [LLVMPointerType(LLVMInt8Type(), 0)];
            let printf_type = LLVMFunctionType(
                LLVMInt32Type(),
                printf_param_types.as_mut_ptr(),
                printf_param_types.len() as u32,
                LLVM_TRUE,
            );
            let printf_name = CString::new("printf").unwrap();
            let printf = LLVMAddFunction(
                self.module,
                printf_name.as_ptr(),
                printf_type,
            );
            let printf_format = LLVMBuildGlobalStringPtr(
                self.builder,
                self.cstr("%lld\n"),
                self.cstr("__PRINTF_FORMAT"),
            );

            let mut globals = HashMap::new();
            for (name, ty) in ast_globals {
                let llvm_type = match ty {
                    Type::Integer => LLVMInt64Type(),
                    Type::Array(first, last) => LLVMArrayType(LLVMInt64Type(), (last - first + 1) as u32),
                };
                let global_ref = LLVMAddGlobal(self.module, llvm_type, self.cstr(&name));
                let initializer = match ty {
                    Type::Integer => LLVMConstInt(LLVMInt64Type(), 0, LLVM_FALSE),
                    Type::Array(first, last) => {
                        // this whole thing just puts `zeroinitializer` in the IR... like srsly?
                        // i spent at least half an hour writing this
                        // and yes, most of it was looking for docs, and no, i didn't find them
                        // intellij FTW ... though every suggestion takes several seconds to appear
                        // maybe if they didn't reimplement half of rustc and used RLS ... :/
                        let mut vals = vec![LLVMConstInt(LLVMInt64Type(), 0, LLVM_FALSE); (last - first + 1) as usize];
                        LLVMConstArray(LLVMInt64Type(), vals.as_mut_ptr(), (last - first + 1) as u32)
                    }
                };
                LLVMSetInitializer(global_ref, initializer);
                globals.insert(name, global_ref);
            }

            let locals = HashMap::new();

            SymbolTable {
                scanf,
                scanf_format,
                printf,
                printf_format,
                consts,
                globals,
                callables: callables.clone(), // HACK
                locals,
            }
        }
    }

    fn alloca_locals<'a, I>(&'a self, ast_vars: I, llvm_vars: &'a mut HashMap<String, LLVMValueRef>)
        where I: IntoIterator<Item=(String, Type)>
    {
        for (name, ty) in ast_vars {
            let c_name = CString::new(name.clone()).unwrap();
            let ptr = unsafe {
                let llvm_type = match ty {
                    Type::Integer => LLVMInt64Type(),
                    _ => {
                        // HACK only global arrays - see fn hack for details
                        unimplemented!()
                    }
                };
                LLVMBuildAlloca(
                    self.builder,
                    llvm_type,
                    c_name.as_ptr(),
                )
            };
            llvm_vars.insert(name.to_owned(), ptr);
        }
    }

    fn hack(stmts: &mut Vec<Stmt>,
            consts: &HashMap<String, Expr>,
            globals: &HashMap<String, Type>,
            starting_locals: &HashMap<String, Type>) -> HashMap<String, Type>
    {
        // i have never felt so dirty thinking about code

        // HACK we don't support shadowing AND all locals are the same type (integer)
        // so we can safely overlap locals with the same name
        let mut all_locals = starting_locals.clone();
        // HACK the above hack would result in variable hoisting
        // but this language was designed in 20 days, not 10,
        // so I have time to do *some* things right
        let mut current_locals = starting_locals.clone();

        fn fix_offset(name: &str, index: &mut Box<Expr>, globals: &HashMap<String, Type>) {
            let offset = if let Type::Array(first, _) = globals.get(name).unwrap() {
                *first
            } else {
                panic!();
            };

            let clone = index.clone(); // HACKception
            *index = Box::new(Expr::Sub(
                clone,
                Box::new(Expr::Const(offset)),
            ));
        }

        fn fix_expr(expr: &mut Expr,
                    consts: &HashMap<String, Expr>,
                    globals: &HashMap<String, Type>,
                    current_locals: &HashMap<String, Type>)
        {
            match expr {
                Expr::Const(_) => {}
                Expr::Neg(ref mut expr) => fix_expr(expr, consts, globals, current_locals),
                Expr::Add(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Sub(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Mul(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Div(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Mod(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }

                Expr::Less(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::LessEquals(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Greater(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::GreaterEquals(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::NotEquals(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Equals(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }

                Expr::Not(ref mut expr) => fix_expr(expr, consts, globals, current_locals),
                Expr::And(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }
                Expr::Or(ref mut expr1, ref mut expr2) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                }

                Expr::Ref(ref name) => {
                    if current_locals.get(name)
                        .or(globals.get(name))
                        .is_none() {
                        if consts.get(name).is_none() {
                            panic!(format!("Invalid variable reference: {}!", name));
                        }
                    }
                }
                Expr::ArrRef(ref name, ref mut index) => {
                    fix_offset(name, index, globals);
                }
                Expr::FnCall(_, ref mut exprs) => {
                    for expr in exprs.iter_mut() {
                        fix_expr(expr, consts, globals, current_locals);
                    }
                }
            }
        };

        fn walk_ast(stmt: &mut Stmt,
                    consts: &HashMap<String, Expr>,
                    globals: &HashMap<String, Type>,
                    all_locals: &mut HashMap<String, Type>,
                    current_locals: &mut HashMap<String, Type>) {
            match stmt {
                Stmt::WriteLn(ref mut expr) => fix_expr(expr, consts, globals, current_locals),
                Stmt::Call(_, ref mut exprs) => {
                    for expr in exprs.iter_mut() {
                        fix_expr(expr, consts, globals, current_locals);
                    }
                }
                Stmt::AssignVar(_, ref mut expr) => fix_expr(expr, consts, globals, current_locals),
                Stmt::AssignArr(ref name, ref mut index, ref mut value) => {
                    fix_expr(index, consts, globals, current_locals);
                    fix_expr(value, consts, globals, current_locals);

                    // and the assignment itself
                    let offset = if let Type::Array(first, _) = globals.get(name).unwrap() {
                        *first
                    } else {
                        panic!();
                    };

                    let clone = index.clone(); // HACKception
                    *index = Expr::Sub(
                        Box::new(clone),
                        Box::new(Expr::Const(offset)),
                    );
                }
                Stmt::Block(ref more_locals, ref mut more_stmts) => {
                    for (local_name, local_type) in more_locals.iter() {
                        all_locals.insert(local_name.clone(), local_type.clone());
                        current_locals.insert(local_name.clone(), local_type.clone());
                    }
                    for stmt in more_stmts.iter_mut() {
                        walk_ast(stmt, consts, globals, all_locals, current_locals);
                    }
                    for (local_name, _) in more_locals.iter() {
                        current_locals.remove(local_name);
                    }
                }
                Stmt::If(ref mut cond, ref mut then_stmt, ref mut else_stmt) => {
                    fix_expr(cond, consts, globals, current_locals);
                    walk_ast(then_stmt, consts, globals, all_locals, current_locals);
                    walk_ast(else_stmt, consts, globals, all_locals, current_locals);
                }
                Stmt::While(ref mut cond, ref mut loop_stmt) => {
                    fix_expr(cond, consts, globals, current_locals);
                    walk_ast(loop_stmt, consts, globals, all_locals, current_locals);
                }
                Stmt::For(_, ref mut expr1, _, ref mut expr2, ref mut loop_stmt) => {
                    fix_expr(expr1, consts, globals, current_locals);
                    fix_expr(expr2, consts, globals, current_locals);
                    walk_ast(loop_stmt, consts, globals, all_locals, current_locals);
                }
                _ => {
                    // others don't contain any Stmts or Exprs
                }
            }
        };

        for stmt in stmts.iter_mut() {
            walk_ast(stmt, consts, globals, &mut all_locals, &mut current_locals);
        }

        all_locals
    }

    pub fn gen(&mut self, mut ast: Program) {
        unsafe {
            let main_type = LLVMFunctionType(LLVMInt64Type(), ptr::null_mut(), 0, LLVM_FALSE);
            let main = LLVMAddFunction(self.module, self.cstr("main"), main_type);

            let bb = LLVMAppendBasicBlockInContext(self.context, main, self.cstr("entry"));
            LLVMPositionBuilderAtEnd(self.builder, bb);

            // apparently prep_symbols needs a basic block and a builder at its end so this needs to be here
            let mut symbols = self.prep_symbols(ast.consts.clone(), ast.globals.clone(), &ast.callables);

            // first create all, then build their instructions to support indirect recursion
            // or even normal function calls since order of functions shouldn't matter in decent langs
            // (not that mila has any chance of being decent as long as it's based on pascal)
            for (name, callable) in symbols.callables.iter() {
                let ret_type = if callable.sig.function { LLVMInt64Type() } else { LLVMVoidType() };
                let mut param_types: Vec<_> = callable.sig.params.iter().map(|_| LLVMInt64Type()).collect();
                let param_types = param_types.as_mut_ptr();
                let param_count = callable.sig.params.len() as u32;
                let callable_type = LLVMFunctionType(ret_type, param_types, param_count, LLVM_FALSE);
                let _func = LLVMAddFunction(self.module, self.cstr(name), callable_type);
            }

            for (name, callable) in ast.callables {
                self.cur_fn_name = name.clone();
                // HACK should probably keep the refs myself since this can silently return null if it's not found
                let func = LLVMGetNamedFunction(self.module, self.cstr(&name));
                assert_ne!(func, ptr::null_mut());

                let bb = LLVMAppendBasicBlockInContext(self.context, func, self.cstr(&name));
                LLVMPositionBuilderAtEnd(self.builder, bb);

                let mut local_symbols = symbols.clone(); // HACK inefficient, potential shadowing

                // Mila supports storing into params but LLVM doesn't so let's create locals for them.
                // Alloca must be at the top of the function, so it's easiest to do it for all params,
                // rather then detecting which are used and which aren't.
                self.alloca_locals(callable.sig.params, &mut local_symbols.locals);

                self.alloca_locals(callable.locals, &mut local_symbols.locals);

                if callable.sig.function {
                    let ret_ptr = LLVMBuildAlloca(self.builder, LLVMInt64Type(), self.cstr(&name));
                    local_symbols.locals.insert(name.to_owned(), ret_ptr);
                }

                let mut param = LLVMGetFirstParam(func);
                for (param_name, param_ty) in local_symbols.callables[&self.cur_fn_name].sig.params.iter() {
                    match param_ty {
                        Type::Integer => {
                            LLVMBuildStore(
                                self.builder,
                                param,
                                local_symbols.locals[param_name],
                            );
                        }
                        _ => unimplemented!(),
                    }
                    param = LLVMGetNextParam(param);
                }

                for stmt in callable.stmts.iter() {
                    self.gen_stmt(&local_symbols, func, stmt);
                }

                if callable.sig.function {
                    LLVMBuildRet(
                        self.builder,
                        self.gen_expr(
                            &local_symbols,
                            &Expr::Ref(name.to_owned()),
                        ));
                } else {
                    LLVMBuildRetVoid(self.builder);
                }
            }

            self.cur_fn_name = "main".to_owned();

            // now set the builder at the end of main again
            LLVMPositionBuilderAtEnd(self.builder, bb);

            let all_locals = Self::hack(&mut ast.stmts, &ast.consts, &ast.globals, &HashMap::new());
            self.alloca_locals(all_locals, &mut symbols.locals);
            //println!("{:#?}", ast.stmts);

            for stmt in ast.stmts.iter() {
                self.gen_stmt(&symbols, main, stmt);
            }

            let ret_val = LLVMConstInt(LLVMInt64Type(), 0, LLVM_FALSE);
            LLVMBuildRet(self.builder, ret_val);

            // TODO see if there's a way to make this work
            //self.print();
            //self.verify();
        }
    }

    fn gen_to_bool(&mut self, symbols: &SymbolTable, expr: &Expr) -> LLVMValueRef {
        unsafe {
            let llvm_ref = self.gen_expr(symbols, expr);
            let zero = LLVMConstInt(LLVMTypeOf(llvm_ref), 0, LLVM_FALSE);
            LLVMBuildICmp(
                self.builder,
                LLVMIntPredicate::LLVMIntNE,
                llvm_ref,
                zero,
                self.cstr("and_ne_tmp_l"),
            )
        }
    }

    fn gen_stmt(&mut self, symbols: &SymbolTable, cur_fn: LLVMValueRef, stmt: &Stmt) {
        match *stmt {
            Stmt::Write(_) => {
                unimplemented!()
            }

            Stmt::WriteLn(ref expr) => {
                let expr_res = self.gen_expr(symbols, &expr);
                self.gen_printf(symbols, expr_res);
            }

            Stmt::ReadLn(ref dest) => {
                let scanf_dest = *symbols.locals.get(dest)
                    .or(symbols.globals.get(dest))
                    .unwrap();
                self.gen_scanf(symbols, scanf_dest);
            }

            Stmt::Call(ref callable_name, ref args) => {
                symbols.callables.get(callable_name).expect(&format!("Unknown function: {}", callable_name));
                unsafe {
                    let func = LLVMGetNamedFunction(self.module, self.cstr(callable_name));
                    assert_ne!(func, ptr::null_mut());
                    let mut args: Vec<_> = args.iter().map(|e| self.gen_expr(symbols, e)).collect();
                    LLVMBuildCall(
                        self.builder,
                        func,
                        args.as_mut_ptr(),
                        args.len() as u32,
                        self.cstr(""),
                    );
                }
            }

            Stmt::AssignVar(ref name, ref expr) => {
                self.gen_assignment(symbols, name, expr);
            }

            Stmt::AssignArr(ref name, ref index, ref value) => {
                let mut array_ptr = symbols.locals.get(name).or(symbols.globals.get(name)).unwrap();
                // if i understand correctly, the global array is actually a pointer
                // so we first need to index into that (hence the 0) and then into the array itself
                // more here: https://llvm.org/docs/GetElementPtr.html
                let indices = &mut [self.gen_expr(symbols, &Expr::Const(0)), self.gen_expr(symbols, index)];
                unsafe {
                    let ptr = LLVMBuildInBoundsGEP(self.builder,
                                                   *array_ptr,
                                                   indices.as_mut_ptr(),
                                                   indices.len() as u32,
                                                   self.cstr("assign_arr"));
                    LLVMBuildStore(self.builder,
                                   self.gen_expr(symbols, value),
                                   ptr);
                }
            }

            Stmt::Block(_, ref stmts) => {
                for stmt in stmts {
                    self.gen_stmt(symbols, cur_fn, stmt);
                }
            }

            Stmt::If(ref expr, ref then_stmt, ref else_stmt) => {
                unsafe {
                    let then_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("then"));
                    let else_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("else"));
                    let merge_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("merge"));

                    let cond = self.gen_to_bool(symbols, &expr);
                    LLVMBuildCondBr(self.builder, cond, then_block, else_block);

                    LLVMPositionBuilderAtEnd(self.builder, then_block);
                    self.gen_stmt(symbols, cur_fn, then_stmt.as_ref());
                    LLVMBuildBr(self.builder, merge_block);

                    LLVMPositionBuilderAtEnd(self.builder, else_block);
                    self.gen_stmt(symbols, cur_fn, else_stmt.as_ref());
                    LLVMBuildBr(self.builder, merge_block);

                    LLVMPositionBuilderAtEnd(self.builder, merge_block);
                }
            }

            Stmt::While(ref expr, ref block) => {
                unsafe {
                    // old code, not tested - naive implementation (condition at the top)
                    // also no break/continue support
                    /*let cond_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("while"));
                    let do_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("do"));
                    let skip_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("skip"));

                    LLVMPositionBuilderAtEnd(self.builder, cond_block);
                    let cond = self.gen_to_bool(symbols, &expr);
                    LLVMBuildCondBr(self.builder, cond, do_block, skip_block);

                    LLVMPositionBuilderAtEnd(self.builder, do_block);
                    self.gen_stmt(symbols, cur_fn, block);
                    LLVMBuildBr(self.builder, cond_block);

                    LLVMPositionBuilderAtEnd(self.builder, skip_block);*/

                    // supposedly better (condition at the bottom)
                    let do_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("do"));
                    let cond_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("cond"));
                    let skip_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("skip"));

                    self.break_dests.push(skip_block);
                    self.continue_dests.push(cond_block);

                    LLVMBuildBr(self.builder, cond_block);

                    LLVMPositionBuilderAtEnd(self.builder, do_block);
                    self.gen_stmt(symbols, cur_fn, block);
                    LLVMBuildBr(self.builder, cond_block);

                    LLVMPositionBuilderAtEnd(self.builder, cond_block);
                    let cond = self.gen_to_bool(symbols, &expr);
                    LLVMBuildCondBr(self.builder, cond, do_block, skip_block);

                    LLVMPositionBuilderAtEnd(self.builder, skip_block);

                    self.break_dests.pop();
                    self.continue_dests.pop();
                }
            }

            Stmt::For(ref var, ref first, dir, ref last, ref block) => {
                // this is like an ugly while
                // could probably unify both loop types using an infinite loop and breaks like rustc does

                self.gen_assignment(symbols, var, first);

                // HACK all of the clones below would be unnecessary if i did this right
                let step = Stmt::AssignVar(
                    var.clone(),
                    Expr::Add(
                        Box::new(Expr::Ref(var.clone())),
                        Box::new(Expr::Const(dir))));

                let l = Box::new(Expr::Ref(var.clone()));
                let r = Box::new(last.clone());
                let cond_expr = if dir == 1 { // counting up
                    Expr::LessEquals(l, r)
                } else { // counting down
                    Expr::GreaterEquals(l, r)
                };

                unsafe {
                    let do_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("do"));
                    let step_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("step"));
                    let cond_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("cond"));
                    let skip_block = LLVMAppendBasicBlockInContext(self.context, cur_fn, self.cstr("skip"));

                    self.break_dests.push(skip_block);
                    self.continue_dests.push(step_block);

                    LLVMBuildBr(self.builder, cond_block);

                    LLVMPositionBuilderAtEnd(self.builder, do_block);
                    self.gen_stmt(symbols, cur_fn, block);
                    LLVMBuildBr(self.builder, step_block);

                    LLVMPositionBuilderAtEnd(self.builder, step_block);
                    self.gen_stmt(symbols, cur_fn, &step);
                    LLVMBuildBr(self.builder, cond_block);

                    LLVMPositionBuilderAtEnd(self.builder, cond_block);
                    let cond = self.gen_to_bool(symbols, &cond_expr);
                    LLVMBuildCondBr(self.builder, cond, do_block, skip_block);

                    LLVMPositionBuilderAtEnd(self.builder, skip_block);

                    self.break_dests.pop();
                    self.continue_dests.pop();
                }
            }

            Stmt::Break => {
                unsafe {
                    LLVMBuildBr(self.builder, *self.break_dests.last().unwrap());
                }
            }

            Stmt::Continue => {
                unsafe {
                    LLVMBuildBr(self.builder, *self.continue_dests.last().unwrap());
                }
            }

            Stmt::Exit => {
                let cur_fn_name = self.cur_fn_name.to_owned();
                if symbols.callables[&cur_fn_name].sig.function {
                    unsafe {
                        LLVMBuildRet(
                            self.builder,
                            self.gen_expr(
                                symbols,
                                &Expr::Ref(cur_fn_name), // HACK, see, it would be really great if codegen didn't generate more AST, this already bit me once
                            ));
                    }
                } else {
                    unsafe { LLVMBuildRetVoid(self.builder); }
                }
            }
        }
    }

    fn gen_scanf(&mut self, symbols: &SymbolTable, read_to_ptr: LLVMValueRef) {
        unsafe {
            let mut args = [
                symbols.scanf_format,
                read_to_ptr
            ];
            LLVMBuildCall(
                self.builder,
                symbols.scanf,
                args.as_mut_ptr(),
                args.len() as u32,
                self.cstr("scanf"),
            );
        }
    }

    fn gen_printf(&mut self, symbols: &SymbolTable, val: LLVMValueRef) {
        unsafe {
            let mut args = [
                symbols.printf_format,
                val
            ];
            LLVMBuildCall(
                self.builder,
                symbols.printf,
                args.as_mut_ptr(),
                args.len() as u32,
                self.cstr("printf_result"),
            );
        }
    }

    fn gen_assignment(&mut self, symbols: &SymbolTable, var_name: &str, expr: &Expr) {
        match symbols.locals.get(var_name).or(symbols.globals.get(var_name)) {
            Some(llvm_ref) => {
                unsafe {
                    LLVMBuildStore(
                        self.builder,
                        self.gen_expr(symbols, expr),
                        *llvm_ref,
                    );
                }
            }
            None => {
                println!("Unknown variable name: {}", var_name);
                panic!()
            }
        }
    }

    fn gen_expr(&mut self, symbols: &SymbolTable, expr: &Expr) -> LLVMValueRef {
        unsafe {
            match *expr {
                Expr::Const(n) => {
                    LLVMConstInt(LLVMInt64Type(), n as u64, LLVM_FALSE)
                }

                Expr::Neg(ref v) => {
                    let v = self.gen_expr(symbols, v.as_ref());

                    LLVMBuildNeg(self.builder, v, self.cstr("tmp_neg"))
                }

                Expr::Add(ref l, ref r) => {
                    let l = self.gen_expr(symbols, l.as_ref());
                    let r = self.gen_expr(symbols, r.as_ref());

                    let name = CString::new("tmp_add").unwrap();
                    LLVMBuildAdd(self.builder, l, r, name.as_ptr())
                }

                Expr::Sub(ref l, ref r) => {
                    let l = self.gen_expr(symbols, l.as_ref());
                    let r = self.gen_expr(symbols, r.as_ref());

                    let name = CString::new("tmp_sub").unwrap();
                    LLVMBuildSub(self.builder, l, r, name.as_ptr())
                }

                Expr::Mul(ref l, ref r) => {
                    let l = self.gen_expr(symbols, l.as_ref());
                    let r = self.gen_expr(symbols, r.as_ref());

                    let name = CString::new("tmp_mul").unwrap();
                    LLVMBuildMul(self.builder, l, r, name.as_ptr())
                }

                Expr::Div(ref l, ref r) => {
                    let l = self.gen_expr(symbols, l.as_ref());
                    let r = self.gen_expr(symbols, r.as_ref());

                    let name = CString::new("tmp_div").unwrap();
                    LLVMBuildSDiv(self.builder, l, r, name.as_ptr())
                }

                Expr::Mod(ref l, ref r) => {
                    let l = self.gen_expr(symbols, l.as_ref());
                    let r = self.gen_expr(symbols, r.as_ref());

                    LLVMBuildSRem(self.builder, l, r, self.cstr("tmp_mod"))
                }

                Expr::Less(ref l, ref r) => {
                    LLVMBuildICmp(
                        self.builder,
                        LLVMIntPredicate::LLVMIntSLT,
                        self.gen_expr(symbols, l.as_ref()),
                        self.gen_expr(symbols, r.as_ref()),
                        self.cstr("less_tmp"),
                    )
                }

                Expr::LessEquals(ref l, ref r) => {
                    LLVMBuildICmp(
                        self.builder,
                        LLVMIntPredicate::LLVMIntSLE,
                        self.gen_expr(symbols, l.as_ref()),
                        self.gen_expr(symbols, r.as_ref()),
                        self.cstr("less_tmp"),
                    )
                }

                Expr::Greater(ref l, ref r) => {
                    LLVMBuildICmp(
                        self.builder,
                        LLVMIntPredicate::LLVMIntSGT,
                        self.gen_expr(symbols, l.as_ref()),
                        self.gen_expr(symbols, r.as_ref()),
                        self.cstr("less_tmp"),
                    )
                }

                Expr::GreaterEquals(ref l, ref r) => {
                    LLVMBuildICmp(
                        self.builder,
                        LLVMIntPredicate::LLVMIntSGE,
                        self.gen_expr(symbols, l.as_ref()),
                        self.gen_expr(symbols, r.as_ref()),
                        self.cstr("less_tmp"),
                    )
                }

                Expr::NotEquals(ref l, ref r) => {
                    LLVMBuildICmp(
                        self.builder,
                        LLVMIntPredicate::LLVMIntNE,
                        self.gen_expr(symbols, l.as_ref()),
                        self.gen_expr(symbols, r.as_ref()),
                        self.cstr("less_tmp"),
                    )
                }

                Expr::Equals(ref l, ref r) => {
                    LLVMBuildICmp(
                        self.builder,
                        LLVMIntPredicate::LLVMIntEQ,
                        self.gen_expr(symbols, l.as_ref()),
                        self.gen_expr(symbols, r.as_ref()),
                        self.cstr("less_tmp"),
                    )
                }

                Expr::Not(ref x) => {
                    let x = self.gen_to_bool(symbols, x.as_ref());
                    LLVMBuildNot(self.builder, x, self.cstr("tmp_not"))
                }

                Expr::And(ref l, ref r) => {
                    let l = self.gen_to_bool(symbols, l.as_ref());
                    let r = self.gen_to_bool(symbols, r.as_ref());

                    LLVMBuildAnd(self.builder, l, r, self.cstr("tmp_and"))
                }

                Expr::Or(ref l, ref r) => {
                    let l = self.gen_to_bool(symbols, l.as_ref());
                    let r = self.gen_to_bool(symbols, r.as_ref());

                    LLVMBuildOr(self.builder, l, r, self.cstr("tmp_or"))
                }

                Expr::Ref(ref name) => {
                    let variable = symbols.locals.get(name).or(symbols.globals.get(name));
                    if let Some(llvm_ref) = variable {
                        LLVMBuildLoad(
                            self.builder,
                            *llvm_ref,
                            self.cstr("tmp_load"),
                        )
                    } else if let Some(expr) = symbols.consts.get(name) {
                        self.gen_expr(symbols, expr)
                    } else {
                        panic!(format!("Unknown identifier: {}", name))
                    }
                }

                Expr::ArrRef(ref name, ref index) => {
                    let mut array_ptr = symbols.locals.get(name).or(symbols.globals.get(name)).unwrap();
                    let indices = &mut [self.gen_expr(symbols, &Expr::Const(0)), self.gen_expr(symbols, index)];
                    let ptr = LLVMBuildInBoundsGEP(self.builder,
                                                   *array_ptr,
                                                   indices.as_mut_ptr(),
                                                   indices.len() as u32,
                                                   self.cstr("assign_arr"));
                    LLVMBuildLoad(self.builder, ptr, self.cstr("tmp_arr_load"))
                }

                Expr::FnCall(ref callable_name, ref args) => {
                    let callable = symbols.callables.get(callable_name).expect(&format!("Unknown function: {}", callable_name));
                    assert!(callable.sig.function);
                    let func = LLVMGetNamedFunction(self.module, self.cstr(callable_name));
                    assert_ne!(func, ptr::null_mut());
                    let mut args: Vec<_> = args.iter().map(|e| self.gen_expr(symbols, e)).collect();
                    LLVMBuildCall(
                        self.builder,
                        func,
                        args.as_mut_ptr(),
                        args.len() as u32,
                        self.cstr("fn_call"),
                    )
                }
            }
        }
    }

    pub fn verify(&self) {
        unsafe {
            LLVMVerifyModule(self.module,
                             LLVMVerifierFailureAction::LLVMAbortProcessAction,
                             ptr::null_mut());
        }
    }

    pub fn print(&self) {
        unsafe {
            LLVMDumpModule(self.module);
        }
    }

    pub fn print_to_file(&self, output_path: &PathBuf) {
        unsafe {
            let mut path_ll = output_path.clone().into_os_string();
            path_ll.push(".ll");

            // HACK this might fail with non utf-8 paths but i don't know of a better way
            let s = path_ll.into_string().unwrap();
            let out_file = CString::new(s).unwrap();

            // HACK it'll actually try to write into the null ptr on error
            LLVMPrintModuleToFile(self.module, out_file.as_ptr(), ptr::null_mut());
        }
        // HACK maybe use LLVMPrintModuleToString and save to file from rust instead?
        // we shouldn't need to use temporary files but do everything in memory, idk how and the docs are a bit scarce
    }

    pub fn compile_to_executable(output_path: &PathBuf) {
        let mut path_ll = output_path.clone().into_os_string();
        path_ll.push(".ll");
        let status = Command::new("llc")
            .arg("-relocation-model=pic")
            .arg("-filetype=obj")
            .arg(path_ll)
            .status()
            .unwrap();
        assert!(status.success());

        let mut path_o = output_path.clone().into_os_string();
        path_o.push(".o");
        let status = Command::new("gcc")
            .arg(path_o)
            .arg("-o")
            .arg(output_path.as_os_str())
            .status()
            .unwrap();
        assert!(status.success());
    }
}

impl Drop for Codegen {
    fn drop(&mut self) {
        unsafe {
            LLVMDisposeBuilder(self.builder);
            LLVMDisposeModule(self.module);
            LLVMContextDispose(self.context);
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    /*use std::process::{Command, Stdio};
    use std::io::{Read, Write};*/

    // HACK this is supposed to be a minimal test cast to find out why llvm-sys generates something different than normal C bindings
    // still needs to be trimmed down or maybe there's a faster way to find out what's happening
    #[test]
    #[ignore]
    fn codegen_debug() {
        unsafe {
            let builder: LLVMBuilderRef = LLVMCreateBuilder();

            let str = CString::new("module-c").unwrap();
            let module: LLVMModuleRef = LLVMModuleCreateWithName(str.as_ptr());

            let mut printf_param_types: [LLVMTypeRef; 1] = [LLVMPointerType(LLVMInt8Type(), 0)];
            let printf_type: LLVMTypeRef = LLVMFunctionType(
                LLVMInt32Type(),
                printf_param_types.as_mut_ptr(),
                0,
                1, // IsVarArg
            );

            let str = CString::new("printf").unwrap();
            let printf_fn: LLVMValueRef = LLVMAddFunction(module, str.as_ptr(), printf_type);

            let mut main_params: [LLVMTypeRef; 0] = [];
            let main_fn_type: LLVMTypeRef = LLVMFunctionType(
                LLVMVoidType(),
                main_params.as_mut_ptr(),
                0,
                0,
            );

            let str = CString::new("main").unwrap();
            let main_function: LLVMValueRef = LLVMAddFunction(module, str.as_ptr(), main_fn_type);
            let str = CString::new("entrypoint").unwrap();
            let basic_block: LLVMBasicBlockRef = LLVMAppendBasicBlock(main_function, str.as_ptr());
            LLVMPositionBuilderAtEnd(builder, basic_block);

            let str1 = CString::new("Hello, %s.\n").unwrap();
            let str2 = CString::new("format").unwrap();
            let format: LLVMValueRef = LLVMBuildGlobalStringPtr(
                builder,
                str1.as_ptr(),
                str2.as_ptr(),
            );
            let str = CString::new("World").unwrap();
            let world: LLVMValueRef = LLVMBuildGlobalStringPtr(
                builder,
                str.as_ptr(),
                str.as_ptr(),
            );

            let mut printf_args: [LLVMValueRef; 2] = [format, world];
            let str = CString::new("printf").unwrap();
            LLVMBuildCall(
                builder,
                printf_fn,
                printf_args.as_mut_ptr(),
                2,
                str.as_ptr(),
            );

            LLVMBuildRetVoid(builder);

            LLVMDumpModule(module);

            LLVMDisposeModule(module);
        }

        /*use ast::*;
        let mut codegen = Codegen::new();
        codegen.test();
        codegen.print();*/
        /*codegen.print_to_file();
        Codegen::compile_to_executable();
        let mut child = Command::new("./mila-out")
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();
        child.stdin.as_mut().unwrap().write("21 22\n".as_ref()).unwrap();
        let mut output = String::new();
        child.stdout.as_mut().unwrap().read_to_string(&mut output).unwrap();
        assert_eq!(output, "21\n22\n");
        let status = child.wait().unwrap();
        assert_eq!(status.code().unwrap(), 50);*/
    }
}
