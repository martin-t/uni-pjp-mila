extern crate uuid;

pub mod utils;
pub mod lexer;
pub mod parser;
pub mod ast;
pub mod codegen;

use std::error::Error;
use std::path::{PathBuf, Path};

use lexer::Lexer;
use parser::Parser;
use codegen::Codegen;

pub fn compile_path<P: AsRef<Path>>(path: P) -> Result<(), Box<Error>> {
    compile_code(&utils::load_file(path)?, &PathBuf::from("mila-out"));
    Ok(())
}

fn compile_code(source: &str, name: &PathBuf) {
    let mut lexer = Lexer::from_code(&source).peekable(); // HACK explicit peekable on lexer needs to die
    let mut parser = Parser::new(&mut lexer);
    let ast = parser.parse();
    //println!("{:#?}", ast);
    let mut codegen = Codegen::new();
    codegen.gen(ast);
    codegen.print_to_file(name);
    Codegen::compile_to_executable(name);
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::env;
    use std::io::{Read, Write};
    use std::process::{Command, Stdio};

    #[test]
    fn check_math() {
        let check_expr = |expr, res| {
            let src = format!("program math; begin writeln({}) end.", expr);
            test_with_numbers(&src, vec![], vec![res]);
        };

        check_expr("1", 1);
        check_expr("1-2", -1);
        check_expr("100-50-20-10-5-1", 14);
        check_expr("100-50/5/2-4", 91);

        check_expr("(0)", 0);
        check_expr("256/2/(200-72)", 1);

        check_expr("3+5", 8);
        check_expr("1+5*10/2", 26);

        check_expr("99 div 11 / 3 + 2", 5);

        check_expr("-10", -10);
        check_expr("-10-5-3", -18);
        check_expr("9+(-4)*6/(-3)", 17);

        check_expr("9 mod 2", 1);
        check_expr("1 = 1", 1);
        check_expr("5 < (3+3)", 1);
        check_expr("2+2 <> 4", 0);

        check_expr("(8-18)*5", -50);
        check_expr("-1/(-1)", 1);
        check_expr("-1/1", -1);

        check_expr("not 0", 1);
        check_expr("not 1", 0);

        check_expr("0 and 0", 0);
        check_expr("0 and 1", 0);
        check_expr("1 and 0", 0);
        check_expr("1 or 1", 1);

        check_expr("0 or 0", 0);
        check_expr("0 or 1", 1);
        check_expr("1 or 0", 1);
        check_expr("1 or 1", 1);

        check_expr("not 0 and (not (not 1))", 1);

        // ^ keep in sync with tests in parser
    }

    // run original samples:
    // =====================

    #[test]
    fn run_array_max() {
        let src = utils::load_file("samples/arrayMax.p").unwrap();
        test_with_numbers(&src, vec![], vec![11, 66, 128, 49, 133, 46, 15, 87, 55, 37, 78, 44, 33, 38, 85, 6, 150, 4, 1, 55, 78, 150]);
    }

    #[test]
    fn run_array_test() {
        let src = utils::load_file("samples/arrayTest.p").unwrap();
        test_with_numbers(&src, vec![1, -20], vec![-20]);
        test_with_numbers(&src, vec![1, 20], vec![20]);
        test_with_numbers(&src, vec![2, -20, 20], vec![0]);
        test_with_numbers(&src, vec![11, -20, -19, -18, -17, -16, -15, -14, -13, -12, -11, -10], vec![-15]);
        test_with_numbers(&src, vec![4, -20, 20, 20, 20], vec![10]);
    }

    #[test]
    fn run_consts() {
        let src = utils::load_file("samples/consts.p").unwrap();
        test_with_numbers(&src, vec![], vec![10, 16, 8]);
    }

    #[test]
    fn run_expressions() {
        let src = utils::load_file("samples/expressions.p").unwrap();
        test_with_numbers(&src, vec![-5], vec![-20]);
        test_with_numbers(&src, vec![-1], vec![0]);
        test_with_numbers(&src, vec![0], vec![5]);
        test_with_numbers(&src, vec![1], vec![10]);
        test_with_numbers(&src, vec![5], vec![30]);
    }

    #[test]
    fn run_expressions2() {
        let src = utils::load_file("samples/expressions2.p").unwrap();
        let test = |x: i64, y: i64| {
            let a = x + y;
            let b = y - x;
            let c = (x + a) * (y - b);
            let d = a % b;
            test_with_numbers(&src, vec![x, y], vec![x, y, a, b, c, d]);
        };
        test(-10, -5);
        test(-10, 5);
        test(10, -5);
        test(10, 5);
        test(17, 11);
    }

    #[test]
    fn run_factorial() {
        let src = utils::load_file("samples/factorial.p").unwrap();
        test_with_numbers(&src, vec![], vec![120, 120]);
    }

    #[test]
    fn run_factorial_cycle() {
        let src = utils::load_file("samples/factorialCycle.p").unwrap();
        test_with_numbers(&src, vec![1], vec![1]);
        test_with_numbers(&src, vec![2], vec![2]);
        test_with_numbers(&src, vec![3], vec![6]);
        test_with_numbers(&src, vec![4], vec![24]);
        test_with_numbers(&src, vec![5], vec![120]);
    }

    #[test]
    fn run_factorial_rec() {
        let src = utils::load_file("samples/factorialRec.p").unwrap();
        test_with_numbers(&src, vec![0], vec![1]);
        test_with_numbers(&src, vec![1], vec![1]);
        test_with_numbers(&src, vec![2], vec![2]);
        test_with_numbers(&src, vec![3], vec![6]);
        test_with_numbers(&src, vec![4], vec![24]);
        test_with_numbers(&src, vec![5], vec![120]);
        test_with_numbers(&src, vec![6], vec![720]);
    }

    #[test]
    #[ignore]
    fn run_factorization() {
        // TODO
    }

    #[test]
    fn run_fibonacci() {
        let src = utils::load_file("samples/fibonacci.p").unwrap();
        test_with_numbers(&src, vec![], vec![21, 34]);
    }

    #[test]
    fn run_gcd() {
        let src = utils::load_file("samples/gcd.p").unwrap();
        test_with_numbers(&src, vec![], vec![27, 27, 27]);
    }

    #[test]
    fn run_indirect_recursion() {
        let src = utils::load_file("samples/indirectrecursion.p").unwrap();
        test_with_numbers(&src, vec![], vec![0, 1]);
    }

    #[test]
    fn run_input_output() {
        let src = utils::load_file("samples/inputOutput.p").unwrap();
        test_with_numbers(&src, vec![-5555], vec![-5555]);
        test_with_numbers(&src, vec![10000000000], vec![10000000000]);
        test_with_numbers(&src, vec![-99910000000000], vec![-99910000000000]);
    }

    #[test]
    fn run_is_prime() {
        let src = utils::load_file("samples/isprime.p").unwrap();
        test_with_numbers(&src, vec![], vec![0, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 1]);
    }

    #[test]
    fn run_sort_bubble() {
        let src = utils::load_file("samples/sortBubble.p").unwrap();
        let expected = vec![20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0,
                            0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
        test_with_numbers(&src, vec![], expected);
    }

    // run my samples:
    // ===============

    #[test]
    fn run_assign() {
        let src = utils::load_file("my_samples/assign.p").unwrap();
        test_with_numbers(&src, vec![], vec![]);
    }

    #[test]
    fn run_break() {
        let src = utils::load_file("my_samples/break.p").unwrap();
        test_with_numbers(&src, vec![], vec![1, 101, 2, 11, 111, 12]);
    }

    #[test]
    fn run_consts_vars() {
        let src = utils::load_file("my_samples/consts_vars.p").unwrap();
        test_with_numbers(&src, vec![], vec![]);
    }

    #[test]
    fn run_continue() {
        let src = utils::load_file("my_samples/continue.p").unwrap();
        test_with_numbers(&src, vec![], vec![1, 101, 2, 3, 103, 11, 111, 12, 13, 113]);
    }

    #[test]
    fn run_empty() {
        let src = utils::load_file("my_samples/empty.p").unwrap();
        test_with_numbers(&src, vec![], vec![]);
    }

    #[test]
    fn run_factorize() {
        let src = utils::load_file("my_samples/factorize.p").unwrap();
        let factors = vec![0, 0,
                           1, 1,
                           2, 2,
                           3, 3,
                           4, 2, 2,
                           5, 5,
                           6, 2, 3,
                           7, 7,
                           8, 2, 2, 2,
                           9, 3, 3,
                           10, 2, 5,
                           11, 11,
                           12, 2, 2, 3,
                           13, 13,
                           14, 2, 7,
                           15, 3, 5,
                           16, 2, 2, 2, 2,
                           17, 17,
                           100, 2, 2, 5, 5,
                           131, 131,
                           133, 7, 19];
        test_with_numbers(&src, vec![], factors);
    }

    #[test]
    fn run_for() {
        let src = utils::load_file("my_samples/for.p").unwrap();
        test_with_numbers(&src, vec![10, 12, 20, 18], vec![1, 10, 11, 12, 2, 20, 19, 18, 3, 10, 11, 12, 4, 20, 19, 18, 5]);
        test_with_numbers(&src, vec![10, 9, 20, 21], vec![1, 10, 11, 12, 2, 20, 19, 18, 3, 4, 5]);
    }

    #[test]
    fn run_globals() {
        let src = utils::load_file("my_samples/globals.p").unwrap();
        test_with_numbers(&src, vec![4], vec![4, 4, 8, 8, 8, 17, 17]);
    }

    #[test]
    fn run_ifs() {
        let src = utils::load_file("my_samples/ifs.p").unwrap();
        test_with_numbers(&src, vec![1, 0], vec![1, 1, 3, 11, 21]);
        test_with_numbers(&src, vec![1, 1], vec![1, 1, 3, 10, 20, 1]);
    }

    #[test]
    fn run_more_consts() {
        let src = utils::load_file("my_samples/more_consts.p").unwrap();
        test_with_numbers(&src, vec![], vec![]);
    }

    #[test]
    fn run_more_expressions() {
        let src = utils::load_file("my_samples/more_expressions.p").unwrap();
        test_with_numbers(&src, vec![], vec![1, 0, 0, 0, 1, 1, 1, 0, 1, -2]);
    }

    #[test]
    fn run_nested_scopes_good() {
        let src = utils::load_file("my_samples/nested_scopes_good.p").unwrap();
        test_with_numbers(&src, vec![1, 2, 3, 4], vec![1, 2, 1, 3, 1, 4, 3, 1, 3, 1, 1]);
    }

    #[test]
    #[should_panic] // HACK - should test for exact error
    fn run_nested_scopes_bad() {
        let src = utils::load_file("my_samples/nested_scopes_bad.p").unwrap();
        test_with_numbers(&src, vec![], vec![]);
    }

    #[test]
    fn run_not() {
        let src = utils::load_file("my_samples/not.p").unwrap();
        test_with_numbers(&src, vec![0], vec![1, 2]);
        test_with_numbers(&src, vec![1], vec![1]);
    }

    #[test]
    fn run_readln_writeln() {
        let src = utils::load_file("my_samples/readln_writeln.p").unwrap();
        test_with_numbers(&src, vec![100, 101], vec![100, 10]);
    }

    #[test]
    fn run_vars() {
        let src = utils::load_file("my_samples/vars.p").unwrap();
        test_with_numbers(&src, vec![], vec![]);
    }

    fn test_with_numbers(source: &str, ins: Vec<i64>, outs: Vec<i64>) {
        let mut i = String::new();
        for num in ins {
            i.push_str(&(num.to_string() + "\n"));
        }

        let mut o = String::new();
        for num in outs {
            o.push_str(&(num.to_string() + "\n"));
        }

        test_with_io(source, &i, &o);
    }

    fn test_with_io(source: &str, i: &str, o: &str) {
        use uuid::prelude::*;

        let mut tmp_path = env::temp_dir();
        tmp_path.push("mila-out-".to_owned() + &Uuid::new_v4().to_string());

        compile_code(source, &tmp_path);
        let mut child = Command::new(&tmp_path)
            .stdin(Stdio::piped())
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();
        child.stdin.as_mut().unwrap().write(i.as_ref()).unwrap();
        let mut output = String::new();
        child.stdout.as_mut().unwrap().read_to_string(&mut output).unwrap();
        let status = child.wait().unwrap();
        assert_eq!(status.code().unwrap(), 0);
        assert_eq!(output, o);
    }
}
