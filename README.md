Compiler for the BI-PJP class at FIT CTU
========================================

So, umm, this is not code I am proud of. Half of it was written in a hurry a year ago when I barely knew rust, ther other half this year (when I had a better idea how to bend the language to my will) in no less of a hurry. Nonetheless, the docs of LLVM's C interface are so bad this might help someone who's also stuck with it.

The extra ugly parts are marked with TODO (those I thought I'd have time to fix) or HACK (those I knew I wouldn't). I didn't have time to fix either.

The mila language is more or less a subset of Pascal and yet it somehow manages to be a worse language than Pascal (mostly by virtue of being implemented in 2 weeks).

Installation
------------

You need LLVM's dev package (`llvm-dev` or similar on debian) that matches the version in Cargo.toml. After that just `cargo build/run/test/whatever`.

Licence
-------

The files under `samples` are provided by [FIT CTU](https://fit.cvut.cz/).

The grammar parser under `grammar/parsingtbl.php` is a modified of version of the one available [here](https://users.fit.cvut.cz/~travnja3/BI-PJP/).

The rest (all rust code and `my_samples` basically) is under GPLv3+.
